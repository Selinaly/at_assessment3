variable "region" {}

variable "instance_type" {}

# variable "pet_clinic_ami_id" {}

variable "vpc_id" {
    type = string
    description = "VPC ID"
}

variable "key_name" {
    type = string
    description = "JaSS Key pem to SSH"
}

variable "jass_sg_webserver" {
    type = list(string)
    description = "security groups for Pet Clinic webserver"
}

variable "jass_sg_lb" {
    type = list(string)
    description = "security groups for Pet Clinic loadbalancer"
}

variable "private_subnets_id" {
    type = list(string)
    description = "list of VPC's private subnets"
}

variable "public_subnets_id" {
    type = list(string)
    description = "list of VPC's public subnets"
}

variable "dbhost" {}
variable "dbuser" {}
variable "dbpass" {}

/* ------------------------------- FOR MAIN.TF ------------------------------ */
# module "as_lb" {
#     source = ".//modules/as_lb"
#     region = var.region
#     vpc_id = module.vpc.vpc_id
#     key_name = var.key_name
#     jass_sg_webserver = [module.security_groups.webserver_sg]
#     jass_sg_lb = [module.security_groups.loadbalancer_sg]
#     private_subnets_id = [module.vpc.private_subnetA_id,module.vpc.private_subnetB_id,module.vpc.private_subnetC_id]
#     public_subnets_id = [module.vpc.public_subnetA_id,module.vpc.public_subnetB_id,module.vpc.public_subnetC_id]
# }