variable "region" {
    default = "eu-west-1"
}
variable "instance_type" {
    default = "t2.micro"
}
variable "rds_username" {
    default = "admin"
}

variable "key_name" {
    default = "JaSSKey"
}
