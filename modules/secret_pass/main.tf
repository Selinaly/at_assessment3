# resource "aws_secretsmanager_secret_rotation" "example" {
#   secret_id           = aws_secretsmanager_secret.example.id
#   rotation_lambda_arn = aws_lambda_function.example.arn

#   rotation_rules {
#     automatically_after_days = 3
#   }
# }

# resource "aws_secretsmanager_secret" "example" {
#   name = "example"
# }
provider "aws" {
    region = "eu-west-1"
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "J@s$!"
}

resource "aws_db_instance" "jass_db_test" {
  instance_class    = "db.t2.micro"
  engine            = "mysql"
  password          = random_password.password.result
  identifier = "jass_db_test"
  allocated_storage = 20
  max_allocated_storage = 100
  storage_type = "gp2"
  engine_version = "8.0.20"
  name = "JaSS_PC_Database"
  username = "admin"
  skip_final_snapshot = true
}