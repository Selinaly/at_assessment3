# Ansible & Terraform Assessment 3
---
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

---
### Links to Working Apps

[Trello Board](https://trello.com/b/PbM9k7IJ/assessment-3-scrum)

[Jenkins server](http://jass-jenkins.academy.labs.automationlogic.com/login?from=%2F)


[PetClinic Application](http://jass-pc.academy.labs.automationlogic.com/)

---
##  <a name='TableofContents'></a>Table of Contents

<!-- vscode-markdown-toc -->
1. [Introduction](#Introduction)
2. [Infrastructure](#Infrastructure)
3. [Jenkins](#Jenkins)
      
    3.1 [Deployment Pipeline](#DPipeline)
        
      - 3.1.1. [B_rds_passgen](#b)

      - 3.1.2 [0b_repo_a_manager](#0b)
   
      - 3.1.3. [1a_DEPLOY_rds](#1a)
   
      - 3.1.4. [1b_DEPLOY_ami](#1b)
   
      - 3.1.5. [1c_DEPLOY_autoscaling_loadbalancing](#1c)
  
    3.2 [Testing Pipeline](#TPipeline)
      

4. [Configuration of PetClinic AMI](#Configuration)
5. [CD Testing](#Testing)
6. [RDS Restore](#RDS)

***
##  1. <a name='Introduction'></a>Introduction

This is a walkthrough of how to launch a functioning PetClinic application via a CI/CD pipeline on Jenkins.

Focusing on the creation of an automated workflow on the following core architecture:

- Infrastructure as code deployment using Terraform.

- A Jenkins server functioning a CI/CD pipeline. 
  
- capacity, webhooked to this repository.
  
- Scalability, Resilience and Maintainability.
  
- Continuous delivery in the testing of code.

- [Testing] - Slack notifications after each build/job.

### Schema

![infrastructure diagram](./images/Assessment3.jpg)

***
##  2. <a name='Infrastructure'></a>Infrastructure

To build the infrastructure (VPC, networking, jenkins instances) for this application just run the `infra_launch.sh` in the JaSS controller instance.
###### Note: Jenkins AMI were manually provisioned using the scripts found in the jenkins_prov folder.

**Make sure you have the JaSSKey.pem in your .ssh folder**

```
$ ssh -i ~/.ssh/JaSSKey.pem ec2-user@34.245.74.22

$ /home/ec2-user/ASSESSMENT3/at_assessment3/infra_launch.sh
```

This runs a series of commands that will build the following using Terraform 

### Networking:
- VPC
- Private and Public NACLs
- Private and Public Subnets
- Internet Gateway (IGW)
- Route table for Private Subnet
- Security Groups 
    - Home IPs
    - Jenkins
    - Bastion
    - Database 
    - Loadbalancer
    - Webservers

### Jenkins Servers:
  - Master 
  - Worker (which is also used as a bastion)

***
##  3. <a name='Jenkins'></a>Jenkins

Once the infrastructure has been set up you can access jenkins to launch the PetClinic application, which has been configured with a deployment pipeline and a testing pipeline for any future changes in the code that has the option of deployment

[Link to go to Jenkins](http://jass-jenkins.academy.labs.automationlogic.com/login?from=%2F)

### Architecture of Jenkins

![jenkins jobs](./images/Jenkins.jpg)

### 3.1. <a name='DPipeline'></a>Deployment Pipeline

This pipeline is set up for deployment of the PetClinic application after the infrastructure of the network has been created. 

Log in to Jenkins and build in the order of the developemtn pipeline to launch Petclinic.


**Deployment pipeline order:**

1. B_rds_passgen
2. 0b_repo_a_manager
3. 1a_DEPLOY_rds
4. 1b_DEPLOY_ami
5. 1c_DEPLOY_autoscaling_loadbalancing

The scripts run by the Jenkins jobs are found in the `jenkins_jobs` folder.

### Diagram of what each Jenkins Job does

![jenkins jobs](./images/JenkinsJobsData.png)

#### 3.1.1. <a name='b'></a>B_rds_passgen

Generates a random password for the database and which is rotated every other day. A script is run in the `Execute Shell` section.

```Jenkins
./B_rds_passgen
```

#### 3.1.2 <a name='0b'></a>0b_repo_a_manager

Clones the required files from this repository which are used in the deployment of the other jobs. 

The following jobs also git push to this job so the terraform.tfstate to update the terreform.tfstate, so log of terraform builds are up to date with new builds made on jenkins. 

A successful build triggers the creation of the database.

#### 3.1.3. <a name='1a'></a>1a_DEPLOY_rds

Creates a AWS RDS database for the application. >A script is run in the `Execute Shell` section.

```Jenkins
./1a_DEPLOY_rds.sh
```
A successful build triggers the creation of the ami.

#### 3.1.4. <a name='1b'></a>1b_DEPLOY_ami

Creates an AMI of the PetClinic app using packer and feeds the new AMI to the next job. A script is run in the `Execute Shell` section.

```Jenkins
./1b_DEPLOY_ami
```
A successful build triggers the launch of the application.

Within jenkins_jobs: to deactivate packer builds of test ami (job A of testing pipeline) and default pet clinic AMI (job 1b of deployment pipeline) comment out the code within this comment:

```
###################################### START OF DEACTIVATE PC AMI BUILD ####################################
<code>
###################################### END OF DEACTIVATE PC AMI BUILD ######################################
```

#### 3.1.5. <a name='1c'></a>1c_DEPLOY_autoscaling_loadbalancing

Launches the app which is loadbalanced and autoscaled over different availability zones. A script is run in the `Execute Shell` section.

```Jenkins
./1c_DEPLOY_autoscaling_and_loadbalancing
```

####  3.2. <a name='TPipeline'></a>Testing Pipeline

Within Jenkins we have created a testing pipeline that allows you to test a change in the code in the CI pipeline of the master branch that will only be deployed once it is tested.

If the client wishes for the PetClinic app to be updated or changed to meet changing requirements, there are CD tests in place to ensure that this new AMI is fully functioning before being deployed. 

Simply push the changes to the test branch, this pipeline is connected via webhook to trigger the Jenkins testing enviroment to build. 

For Example: 

1. As a developer git checkout into test branch.
2. Edit the file `script.tpl` for changes to AMI.
3. Git push changes, which would trigger the build.

```
$ git checkout test

# change script from terminal or access on local machine
$ nano script.tpl 

$ git add. 
$ git commit -m "add good message"
$ git push origin test
```
#### A_create_pc_ami.sh

The new AMI is again created using packer, this time on the test branch with required adjustments added. When running the packer build command the exit code from this build is copied to the master branch. 

A script is run in the `Execute Shell` section.

```Jenkins
./A_create_pc_ami.sh
```

Following this, an if statement in the deployment pipeline (in the **1b_DEPLOY_ami.sh** job) checks if the exit code of this is equal to 1 (build failure) or 0 (a build success).
If the build is a success then new AMI id is used, if the new build is a failure then the last stable AMI id is continued to be used in the pipeline ( in the **1c_DEPLOY_autoscaling_and_loadbalancing** job). 

![slack notification](./images/SlackNotifications.png)

A slack notification is sent to alert developers if builds have been successful or not.

***
##  5. <a name='Configuration'></a>Configuration of PetClinic AMI

The base AMI is built from the `packerjson.json` file, with the name **jass_pc_ami**.
The creation of the PetClinic AMI is built using packer. The AMI installs the apps dependencies and secure copies in files required for launching of instances for the app.

For more information see the README_petclinic.md in the petclinic folder.

***
##  6. <a name='RDS'></a>RDS Restore

To restore a database snapshot: 

1. Log on to Amazon's console and click on "RDS" 
2. Click "Automated backups" 
3. Click on "jassdb" RDS instance
4. Click on "Actions"  
5. Click on "restore to point in time"
