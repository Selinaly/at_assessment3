#!/bin/bash
# Resets the 0b_repo_a_manager workspace with git clone.

#  create variables from temp files to be used during templating
dbhost=$(cat /home/ec2-user/dbhost)
dbuser=$(cat /home/ec2-user/dbuser)
dbpass=$(cat /home/ec2-user/RDSpass)

cd /home/ec2-user/workspace/A_create_pc_ami/petclinic/
sed -e "s;%HOST%;$dbhost;g" -e "s;%USER%;$dbuser;g" -e "s;%PASS%;$dbpass;g" /home/ec2-user/workspace/A_create_pc_ami/petclinic/script.tpl > /home/ec2-user/workspace/A_create_pc_ami/petclinic/prov_pet_1.sh

# ------------------- working directory of the test branch ------------------- #
# ----------------- using json template builds petclinic ami ----------------- #
# additionally writes stdout to file to be used for slack notifications and autoscaling.
###################################### START OF DEACTIVATE PC AMI BUILD ####################################
cd /home/ec2-user/workspace/A_create_pc_ami/petclinic/
packer build packerjson.json 2>&1 | sudo tee /home/ec2-user/test_output.txt
###################################### END OF DEACTIVATE PC AMI BUILD ######################################

# - adds pkr notification to test_ami_log and posts contents of file to Slack - #
touch /home/ec2-user/test_ami_log
:> /home/ec2-user/test_ami_log
echo -n "packer build packerjson.json executed with exit code: $?" >> /home/ec2-user/test_ami_log
echo -n $(echo "AMI created: $(grep 'eu-west-1: ami-*' /home/ec2-user/test_output.txt) :jenkins_ci: :avocado-man:") >> /home/ec2-user/test_ami_log
#python3 /home/ec2-user/workspace/A_create_pc_ami/python/slacker.py /home/ec2-user/test_ami_log

# ------------------ writes ami id from packer build to file ----------------- #
ami_id=$(echo $(grep 'eu-west-1: ami-*' /home/ec2-user/test_output.txt | sed 's/\x1b\[[0-9;]*m//g') | cut -d ':' -f2 | cut -d ' ' -f2)
echo -n $ami_id > /home/ec2-user/workspace/A_create_pc_ami/modules/as_lb/test_pc_ami.txt 

# --------------------- finds ami id from test_output.txt -------------------- #
grep 'AMIs were created' /home/ec2-user/test_output.txt
echo -n $? > /home/ec2-user/anewbuild
echo -n "0" >/home/ec2-user/anewbuild #update flag of newbuild | used in job 1b in deployment pipeline

# ------ copies test ami_id and newbuildexitcode files to master branch ------ #
sudo cp -a /home/ec2-user/workspace/A_create_pc_ami/modules/as_lb/test_pc_ami.txt /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/modules/as_lb/test_pc_ami.txt
sudo rm /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/modules/as_lb/anewbuild
sudo cp -a /home/ec2-user/anewbuild /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/modules/as_lb/anewbuild

# -------- change directory to working directory of the master branch -------- #
cd /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/
git add .
git commit -m "test ami from test branch"
git push origin master
