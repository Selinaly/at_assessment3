# Install Ansible
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py --user
python3 -m pip install --user ansible
python3 -m pip install --user paramiko
sudo python3 get-pip.py
sudo python3 -m pip install ansible
sudo python3 -m pip install virtualenv
sudo python3 -m virtualenv ansible
source ansible/bin/activate
sudo python3 -m pip install ansible
sudo touch ansible/ansible.cfg
sudo sh -c "sudo curl https://raw.githubusercontent.com/ansible/ansible/devel/examples/ansible.cfg >>ansible.cfg"
deactivate

# Install packer
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install packer

# Install terraform
sudo yum install -y terraform