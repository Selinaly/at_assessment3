variable "region" {}

variable "instance_type" {}

variable "jenkins_ami_manager" {
    type = string
    description = "ami to launch jenkins manager"
}

variable "jenkins_ami_worker" {
    type = string
    description = "ami to launch jenkins worker"
}

variable "jenkins_subnet" {
    type = string
    description = "public subnet 1c that we want jenkins in"
}

variable "jenkins_master_sg" {
    type = list(string)
    description = "JaSS home ips SG and Jenkins master SG"
}

variable "jenkins_worker_sg" {
    type = list(string)
    description = "Jenkins master SG"
}

variable "key_name" {
    type = string
    description = "JaSS Key pem to SSH"
}

variable "iam_instance_profile" {
    type = string
    description = "default AL jenkins IAM role"
}