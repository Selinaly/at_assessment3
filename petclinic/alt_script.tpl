#!/bin/bash
dbpass=%PASS%
dbhost=%HOST%
dbuser=%USER%
cd petclinic
sudo cp /home/ec2-user/petclinic/src/main/resources/application.properties /home/ec2-user/petclinic/src/main/resources/new.properties
sudo sed -i "s,spring.datasource.url=jdbc:mysql://localhost/petclinic, spring.datasource.url=jdbc:mysql://$dbhost/petclinic," /home/ec2-user/petclinic/src/main/resources/application.properties
sudo sed -i "s,spring.datasource.username=petclinic, spring.datasource.username=$dbuser," /home/ec2-user/petclinic/src/main/resources/application.properties
sudo sed -i "s,spring.datasource.password=petclinic, spring.datasource.password=$dbpass," /home/ec2-user/petclinic/src/main/resources/application.properties
sudo mvn -Dmaven.test.skip=true package