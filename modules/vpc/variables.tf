variable "region" {}

variable "vpc_cidr_block" {
    type = string
    description = "VPC cidr block"
}

variable "pub_block_1c" {
    type = string
    description = "public subnet 1c cidr block"
}

variable "pub_block_1a" {
    type = string
    description = "public subnet 1a cidr block"
}

variable "pub_block_1b" {
    type = string
    description = "public subnet 1b cidr block"
}

variable "private_block_1c" {
    type = string
    description = "private subnet 1c cidr block"
}

variable "private_block_1a" {
    type = string
    description = "private subnet 1a cidr block"
}

variable "private_block_1b" {
    type = string
    description = "private subnet 1b cidr block"
}