# Jenkins ec2

resource "aws_instance" "jass_jenkins_manager" {
  #ami = "ami-0635ca8333e27394d"
  #ami = var.HTTPD_AMI
  ami = var.jenkins_ami_manager # JABS jenkins Manager
  iam_instance_profile = var.iam_instance_profile
  instance_type = var.instance_type
  subnet_id = var.jenkins_subnet
  key_name = var.key_name
  associate_public_ip_address = true
  security_groups = var.jenkins_master_sg
  vpc_security_group_ids = var.jenkins_master_sg

  tags = {
    "Name" = "JaSS_Jenkins_manager"
  }
}

resource "aws_instance" "jass_jenkins_worker" {
  #ami = "ami-0635ca8333e27394d"
  #ami = var.HTTPD_AMI
  ami = var.jenkins_ami_worker # JABS jenkins Worker
  iam_instance_profile = var.iam_instance_profile
  instance_type = var.instance_type
  subnet_id = var.jenkins_subnet
  key_name = var.key_name
  associate_public_ip_address = true
  security_groups = [var.jenkins_worker_sg]
  vpc_security_group_ids = var.jenkins_worker_sg

  tags = {
    "Name" = "JaSS_Jenkins_worker"
  }
}
