  provider "aws" {
  shared_credentials_file = "~/.ssh/JaSSKey.pem"
  region = var.region
}