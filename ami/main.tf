resource "aws_instance" "web" {
  ami = data.aws_ami.petclinic_ami.image_id
  instance_type = "t2.micro"
  key_name = "JaSSKey"
  subnet_id = "subnet-02f9ecc8a688a19b4"

  tags = {
    Name = "PC JaSS Terraform"
  }
}

data "aws_ami" "petclinic_ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = ["jass_pc_base_ami"]
  }                              
}