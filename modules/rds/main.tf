# RDS database
data "local_file" "RDSpass" {
  filename = "/home/ec2-user/RDSpass"
}

resource "aws_db_instance" "jass_database" {
  identifier = "jassdb"
  allocated_storage = 20
  max_allocated_storage = 100
  multi_az = true
  apply_immediately = true
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "8.0.20"
  instance_class = "db.t2.micro"
  name = "JaSS_PC_Database"
  username = var.username
  password = data.local_file.RDSpass.content
  skip_final_snapshot = true
  db_subnet_group_name = "jass_db_subnet"
  depends_on = [aws_db_subnet_group.jass_db_subnet]
  vpc_security_group_ids = [var.database_sg]
}

# ------------ Setting up db subnet group -----------------

resource "aws_db_subnet_group" "jass_db_subnet" {
 name       = "jass_db_subnet"
 description = "Group of subnets associated with the database subnet group"
 subnet_ids = ["${var.private_subnetA}", "${var.private_subnetB}", "${var.private_subnetC}"]

 tags = {
   Name = "JaSS DB subnet"
 }
}

# ----------- write db host name to local file ----------
resource "local_file" "dbhost" {
    content     = aws_db_instance.jass_database.address
    filename = "/home/ec2-user/dbhost"
}
# ------------ write username to local file -------------
resource "local_file" "dbuser" {
    content     = aws_db_instance.jass_database.username
    filename = "/home/ec2-user/dbuser"
}
# ------------ write password to local file ---------
resource "local_file" "dbpass" {
    content     = aws_db_instance.jass_database.password
    filename = "/home/ec2-user/dbpass"
}
