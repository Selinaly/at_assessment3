output "database_sg" {
    value = aws_security_group.jass_db.id
}

output "home_ips_sg" {
    value = aws_security_group.jass_sg_home_ips.id
}

output "jenkins_master_sg" {
    value = aws_security_group.jass_sg_jenkins_master.id
}

output "webserver_sg" {
    value = aws_security_group.jass_sg_webserver.id
}

output "loadbalancer_sg" {
    value = aws_security_group.jass_sg_loadbalancer.id
}

output "bastion_sg" {
    value = aws_security_group.jass_sg_bastion.id
}

output "bitbucket_sg" {
    value = aws_security_group.jass_bitbucket.id
}