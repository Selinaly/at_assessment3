output "vpc_id" {
    value = aws_vpc.jass_main_vpc.id
}

output "vpc_cidr_block" {
    value = aws_vpc.jass_main_vpc.cidr_block
}

output "private_subnetA_id" {
    value = aws_subnet.jass_private_subnet_1a.id
}

output "private_subnetB_id" {
    value = aws_subnet.jass_private_subnet_1b.id
}

output "private_subnetC_id" {
    value = aws_subnet.jass_private_subnet.id
}

output "public_subnetC_id" {
    value = aws_subnet.jass_public_subnet.id
}

output "public_subnetA_id" {
    value = aws_subnet.jass_public_subnet_1a.id
}

output "public_subnetB_id" {
    value = aws_subnet.jass_public_subnet_1b.id
}