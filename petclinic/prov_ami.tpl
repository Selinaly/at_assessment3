#!/bin/bash
# Username, password and dbs endpoint
dbpass=%PASS%
dbhost=%HOST%
dbuser=%USER%
# ----- installing git then cloning in the petclinic repo -----
sudo yum -y install git

sudo git clone https://Stephanie_Hyland@bitbucket.org/JangleFett/petclinic.git

cd petclinic
# ----- installing dependencies for petclinic -----
# ----- installing maven and ensuring the path for maven is set correctly -----
cd /opt
sudo wget https://downloads.apache.org/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.tar.gz
sudo tar -xvzf apache-maven-3.8.1-bin.tar.gz

sudo sh -c "echo M2_HOME=\"/opt/apache-maven-3.8.1\" >>/etc/environment"
sudo sh -c "echo PATH=$PATH:/opt/apache-maven-3.8.1/bin >>/etc/environment"
sudo sed -i "s,PATH=\$PATH,PATH=$PATH:/opt/apache-maven-3.8.1/bin,"  /etc/environment
sudo update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/apache-maven-3.8.1/bin/mvn" 0
sudo update-alternatives --set mvn /opt/apache-maven-3.8.1/bin/mvn
sudo wget https://raw.github.com/dimaj/maven-bash-completion/master/bash_completion.bash --output-document /etc/bash_completion.d/mvn
# ----- installing the version of java needed setting path for java ------
cd 
sudo curl -L -C - -b "oraclelicense=accept-securebackup-cookie" -O "http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz"
sudo mkdir /usr/lib/jvm/
cd /usr/lib/jvm
sudo tar -xvzf ~/jdk-8u131-linux-x64.tar.gz
sudo sh -c "echo PATH=$PATH:/usr/lib/jvm/jdk1.8.0_131/bin:/usr/lib/jvm/jdk1.8.0_131/db/bin:/usr/lib/jvm/jdk1.8.0_131/jre/bin >>/etc/environment"
sudo sh -c "echo J2SDKDIR=\"/usr/lib/jvm/jdk1.8.0_131\" >>/etc/environment"
sudo sh -c "echo J2REDIR=\"/usr/lib/jvm/jdk1.8.0_131/jre\" >>/etc/environment"
sudo sh -c "echo JAVA_HOME=\"/usr/lib/jvm/jdk1.8.0_131\" >>/etc/environment"
sudo sh -c "echo DERBY_HOME=\"/usr/lib/jvm/jdk1.8.0_131/db\" >>/etc/environment"
# ---- installing mysql and maria db needed for connecting the petclinic app to the RDS database -------
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_131/bin/java" 0
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_131/bin/javac" 0
sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_131/bin/java
sudo update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_131/bin/javac
update-alternatives --list java
update-alternatives --list javac
# ------ installing httpd and starting httpd ------
sudo yum -y install mysql
sudo yum -y install mariadb-server

# sending the data and scheme sql files to the database and creating the user; note these are moved to the user_data as you can't connect to the db when making an AMI
# mysql -h $dbhost -u $dbuser --password="$dbpass" < /home/ec2-user/petclinic/src/main/resources/db/mysql/schema.sql
# mysql -h $dbhost -u $dbuser --password="$dbpass" < /home/ec2-user/petclinic/src/main/resources/db/mysql/data.sql
# mysql -h $dbhost -u $dbuser --password="$dbpass" < /home/ec2-user/create_user.sql

sudo yum -y install httpd
sudo systemctl start httpd


sudo sh -c "echo \"<VirtualHost *:80>
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:8080/
    ProxyPassReverse / http://127.0.0.1:8080/
</VirtualHost>\" > /etc/httpd/conf.d/petclinic.conf"

sudo mv ~/petclinic.init /etc/init.d/petclinic
sudo chmod +x /etc/init.d/petclinic

cd
cd petclinic
# ----- Editiing the application.properties file to contain the correct username, password and db endpoint so that the app can communicate to the rds -----
sudo cp /home/ec2-user/petclinic/src/main/resources/application.properties /home/ec2-user/petclinic/src/main/resources/new.properties
sudo sed -i "s,spring.datasource.url=jdbc:mysql://localhost/petclinic, spring.datasource.url=jdbc:mysql://$dbhost/petclinic," /home/ec2-user/petclinic/src/main/resources/application.properties
sudo sed -i "s,spring.datasource.username=petclinic, spring.datasource.username=$dbuser," /home/ec2-user/petclinic/src/main/resources/application.properties
sudo sed -i "s,spring.datasource.password=petclinic, spring.datasource.password=$dbpass," /home/ec2-user/petclinic/src/main/resources/application.properties
# ----- compiling the petclinic app -----
sudo mvn -Dmaven.test.skip=true package
# ----- reverse proxy for the app to run on port 80 not 8080 -----
cd /etc/httpd/conf.d
# ----- these lines are moved to the user_data for after the AMI is made and then launched -----
#sudo chkconfig --add petclinic
#sudo systemctl enable petclinic
#sudo /etc/init.d/petclinic start
#sudo systemctl restart httpd
