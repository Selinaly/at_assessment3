#!/bin/bash

cd ..
terraform init
terraform apply -auto-approve -target=module.vpc -target=module.security_groups -target=module.jenkins

git add .
git commit -m "launching assessment 3 infrastructure: VPC, networking, and Jenkins resources."
git push origin master