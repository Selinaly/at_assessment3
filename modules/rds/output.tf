output "rds_endpoint" {
    value = aws_db_instance.jass_database.address
}

output "rds_username" {
    value = aws_db_instance.jass_database.username
}

output "rds_password" {
    value = aws_db_instance.jass_database.password
    sensitive   = true
}

output "rds_arn" {
    value = aws_db_instance.jass_database.arn
}