#!/bin/bash 

# -------- change directory to working directory of the master branch -------- #
cd /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/

# - executes tf plan for rds_database module. Creates/Modifies RDS resources - #
# additionally writes the stdout to a file to be used for the slack notification.
# generates dbhost, dbuser, dbpass files that are used to provision packer-generated ami for pet clinic
#terraform plan -target=module.rds_database
terraform apply -target=module.rds_database -auto-approve 2>&1 | sudo tee /home/ec2-user/rds_output

# ---- adds tf notification to rds_log and posts contents of file to Slack --- #
touch /home/ec2-user/rds_log
:> /home/ec2-user/rds_log
# echo "[TERRAFORM] Create RDS executed with exit code: $?" >> /home/ec2-user/rds_log
echo $(echo "[TERRAFORM/RDS] $(grep 'Apply complete' /home/ec2-user/rds_output)") >> /home/ec2-user/rds_log
python3 /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/python/slacker.py /home/ec2-user/rds_log


# --------------- commit and push changes to terraform.tfstate --------------- #
git add .
git commit -m "tfstate change"
git push origin master

