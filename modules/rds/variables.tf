variable "region" {}

variable "username" {
    type = string
    description = "database username"
}

# variable "password" {
#     type = string
#     description = "database password"
# }

variable "private_subnetA" {
    type = string
    description = "private subnet id in availability zone 1a"
}

variable "private_subnetB" {
    type = string
    description = "private subnet id in availability zone 1b"
}

variable "private_subnetC" {
    type = string
    description = "private subnet id in availability zone 1c"
}

variable "database_sg" {
    type = string
    description = "security groups for database"
}
