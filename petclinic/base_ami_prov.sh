#!/bin/bash
#installing git then cloning in the petclinic repo
sudo yum -y install git

sudo git clone https://Stephanie_Hyland@bitbucket.org/JangleFett/petclinic.git

cd petclinic
# installing dependencies for petclinic
# installing maven and ensuring the path for maven is set correctly
cd /opt
sudo wget https://downloads.apache.org/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.tar.gz
sudo tar -xvzf apache-maven-3.8.1-bin.tar.gz

sudo sh -c "echo M2_HOME=\"/opt/apache-maven-3.8.1\" >>/etc/environment"
sudo sh -c "echo PATH=$PATH:/opt/apache-maven-3.8.1/bin >>/etc/environment"
sudo sed -i "s,PATH=\$PATH,PATH=$PATH:/opt/apache-maven-3.8.1/bin,"  /etc/environment
sudo update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/apache-maven-3.8.1/bin/mvn" 0
sudo update-alternatives --set mvn /opt/apache-maven-3.8.1/bin/mvn
sudo wget https://raw.github.com/dimaj/maven-bash-completion/master/bash_completion.bash --output-document /etc/bash_completion.d/mvn
# installing the version of java needed setting path for java
cd 
sudo curl -L -C - -b "oraclelicense=accept-securebackup-cookie" -O "http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz"
sudo mkdir /usr/lib/jvm/
cd /usr/lib/jvm
sudo tar -xvzf ~/jdk-8u131-linux-x64.tar.gz
sudo sh -c "echo PATH=$PATH:/usr/lib/jvm/jdk1.8.0_131/bin:/usr/lib/jvm/jdk1.8.0_131/db/bin:/usr/lib/jvm/jdk1.8.0_131/jre/bin >>/etc/environment"
sudo sh -c "echo J2SDKDIR=\"/usr/lib/jvm/jdk1.8.0_131\" >>/etc/environment"
sudo sh -c "echo J2REDIR=\"/usr/lib/jvm/jdk1.8.0_131/jre\" >>/etc/environment"
sudo sh -c "echo JAVA_HOME=\"/usr/lib/jvm/jdk1.8.0_131\" >>/etc/environment"
sudo sh -c "echo DERBY_HOME=\"/usr/lib/jvm/jdk1.8.0_131/db\" >>/etc/environment"

sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_131/bin/java" 0
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_131/bin/javac" 0
sudo update-alternatives --set java /usr/lib/jvm/jdk1.8.0_131/bin/java
sudo update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_131/bin/javac
update-alternatives --list java
update-alternatives --list javac
# installing mysql and maria db needed for connecting the petclinic app to the RDS database
sudo yum -y install mysql
sudo yum -y install mariadb-server