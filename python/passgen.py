import random
import string
import datetime

timestamp_file = "/home/ec2-user/TimeStamp_4_RDSpass"
password_file = "/home/ec2-user/RDSpass"

def randompasswordgenerator(length):
    # Get the current datetime when the function is called
    timeNow = datetime.datetime.now()

    # assigns a string to variables that only contain appropriate ascii or custom characters
    uletters = string.ascii_uppercase 
    lletters = string.ascii_lowercase
    digits = string.digits
    # special = string.punctuation
    special = '!?'  # custom declared strings for special characters


    # declares empty string: password
    password = ''
    starting_char = '13' # either start with lower case or upper case letter
    
    for i in range(0,length):
        random_number = random.randint(1, 3)
        if i == 0:
            random_number = random.choice(starting_char)
        else:
            random_number = random.randint(1, 3)

        # appends either upper/lower case letters, digits, custom special characters to password
        if random_number == 1:
            password = password + f"{str(random.sample(lletters, 1)[0])}"
        elif random_number == 2:
            password = password + f"{str(random.sample(digits, 1)[0])}"
        elif random_number == 3:
            password = password + f"{str(random.sample(uletters, 1)[0])}"
        elif random_number == 4:
            password = password + f"{str(random.choice(special))}"

    open(timestamp_file, "w").write(str(timeNow)) # save password creation timestamp
    open(password_file, "w+").write(password) # write password to file
    return password


if __name__ == '__main__':
    randompasswordgenerator(12)
    # try:
    #     ### Uses internal timer
    #     # rotation = 2 # currently set to minutes, change to 2 days
    #     # imported_time_timestamp = datetime.datetime.strptime(open(timestamp_file, "r").read(),"%Y-%m-%d %H:%M:%S.%f") # convert previous timestamp to datetime object for comparison
    #     # allow_change_timestamp = imported_time_timestamp + datetime.timedelta(days=rotation) # Sets rotation allowance | currently set to minutes, change to 2 days
    #     # current_timestamp = datetime.datetime.now() # gets current datetime
    #     # if current_timestamp >= allow_change_timestamp: # compares the current timestamp to the previous timestamp, if TRUE, password rotation is executed.
    #     #     randompasswordgenerator(10)
    #     #     print('RDS password changed')
    #     # else:
    #     #     print('RDS password change not allowed')


    # except FileNotFoundError:
    #     print('--------no RDS password found, generating new password-------')
    #     randompasswordgenerator(10)
        