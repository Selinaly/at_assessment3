/* ---------------------------- Create/modify VPC --------------------------- */
# 1 VPC, 6 subnets (3 private and 3 public), 1 internet gateway, 2 route tables (private and public)
#  6 route associations (3 private and 3 public), and public and private NACLs.
module "vpc" {
    source = ".//modules/vpc"
    vpc_cidr_block = "108.52.0.0/16"
    pub_block_1c = "108.52.0.0/24"
    pub_block_1a = "108.52.4.0/24"
    pub_block_1b = "108.52.5.0/24"
    private_block_1c = "108.52.1.0/24"
    private_block_1a = "108.52.2.0/24"
    private_block_1b = "108.52.3.0/24"
    region = var.region
}

/* ---------------------------- create/modify RDS --------------------------- */
#  db instance, db subnet group, localfiles: dbhost, dbpass, dbuser [rds endpoint info]
module "rds_database" {
    source = ".//modules/rds"
    username = var.rds_username
    private_subnetA = module.vpc.private_subnetA_id
    private_subnetB = module.vpc.private_subnetB_id
    private_subnetC = module.vpc.private_subnetC_id
    database_sg = module.security_groups.database_sg
    region = var.region    
}

/* --------------------- create/modify jenkins resources -------------------- */
# jenkins worker instance, jenkins master instance.
module "jenkins" {
    source = ".//modules/jenkins"
    jenkins_ami_manager = "ami-0f31f506bca38f291" # ami-0927d8ca55eca847b
    iam_instance_profile = "ALAcademyJenkinsSuperRole" 
    jenkins_subnet = module.vpc.public_subnetC_id
    jenkins_master_sg = [module.security_groups.home_ips_sg, module.security_groups.jenkins_master_sg, module.security_groups.bitbucket_sg]
    key_name = var.key_name
    instance_type = var.instance_type
    jenkins_ami_worker = "ami-060d2d3130d224737" # ami-083518aa0500e7bd8
    jenkins_worker_sg = [module.security_groups.jenkins_master_sg, module.security_groups.bastion_sg]
    region = var.region
}

/* ---------------------- create/modify security groups --------------------- */
# bitbucket (webhook), home ips (dev ips), bastion (jenkins worker), master,
# database (db), loadbalancer, webservers.
module "security_groups" {
    source = ".//modules/security_groups"
    vpc_id = module.vpc.vpc_id
    region = var.region
}

/* ----------- create/modify autoscaling groups and loadbalancing ----------- */
# launch template, autoscaling group, autoscaling attachment, loadbalancer,
# target group, loadbalancer listener, route 53 record
module "as_lb" {
    source = ".//modules/as_lb"
    region = var.region
    vpc_id = module.vpc.vpc_id
    key_name = var.key_name
    instance_type = var.instance_type
    jass_sg_webserver = [module.security_groups.webserver_sg]
    jass_sg_lb = [module.security_groups.loadbalancer_sg]
    private_subnets_id = [module.vpc.private_subnetA_id,module.vpc.private_subnetB_id,module.vpc.private_subnetC_id]
    public_subnets_id = [module.vpc.public_subnetA_id,module.vpc.public_subnetB_id,module.vpc.public_subnetC_id]
    dbhost = module.rds_database.rds_endpoint
    dbuser = module.rds_database.rds_username
    dbpass = module.rds_database.rds_password
}