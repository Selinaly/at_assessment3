variable "region" {}

variable "vpc_id" {
    type = string
    description = "VPC ID"
}

variable "ssh_port" {
  description = "Default port for SSH protocol"
  default     = "22"
}

variable "open_internet" {
  description = "CIDR block open to the internet"
  default     = [ "0.0.0.0/0" ]
}

variable "outbound_port" {
  description = "Port open to allow outbound connection"
  default     = "0"
}

variable "http_port" {
  description = "Default port for http protocol"
  default     = "80"
}

variable "mysql_port" {
  description = "Default port for mysql protocol"
  default     = "3306"
}

variable "ipv6_cidr_block" {
    description = "ipv6 CIDR block"
  default     = [ "::/0" ]
}
