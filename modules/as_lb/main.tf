# Autoscaling and Loadbalancing
/* ----------------------------- launch template ---------------------------- */
data "local_file" "pet_ami" {
  filename = ".//modules/as_lb/pc_ami.txt"
}

# write to local file
resource "local_file" "pc_ami_tag_write" {
    content     = data.local_file.pet_ami.content
    filename = "/home/ec2-user/ami_tag"
}


data "aws_ami" "pc_ami" {
  most_recent = true
  owners = ["self"]
  filter {                       
    name = "tag:Name"     
    values = [data.local_file.pet_ami.content]
  }                              
}

data "local_file" "DBpass" {
  filename = "/home/ec2-user/RDSpass"
}

resource "aws_launch_template" "pet_ami_template" {
  name  = "jass_pc_template"
  image_id = data.aws_ami.pc_ami.image_id # data.local_file.pet_ami.content #"${file(".//modules/as_lb/pet_clinic_ami.txt")}" #var.pet_clinic_ami_id #### DECLARE IN MAIN
  instance_type = var.instance_type 
  key_name = var.key_name 
  vpc_security_group_ids = var.jass_sg_webserver
  update_default_version = true
  monitoring {
      enabled = true
  }
  tag_specifications {
    resource_type = "instance"
    tags = {
        "Name" = "JaSS_Pet_Clinic"
    }
  }
  # user_data = "${filebase64(".//modules/as_lb/petclinic_start.sh")}"
  user_data = base64encode(templatefile(".//modules/as_lb/petclinic_start.tpl",
  {
    dbhost = "${var.dbhost}"
    dbuser = "${var.dbuser}"
    dbpass = "${var.dbpass}"
  }
  ))
}

/* --------------------------- auto scaling group --------------------------- */
resource "aws_autoscaling_group" "pet_autoscaling_group" {
  name = "jass-pc-autoscaling-group"
  capacity_rebalance  = true
  desired_capacity    = 3
  max_size            = 5
  min_size            = 2
  vpc_zone_identifier = var.private_subnets_id #### MAIN

  tag {
      key = "Name"
      value = "JaSS_Pet_Clinic"
      propagate_at_launch = true
  }
  launch_template {
    id = aws_launch_template.pet_ami_template.id 
    version = "$Default"
  }

  lifecycle {
    ignore_changes = [load_balancers, target_group_arns]
    create_before_destroy = true
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      instance_warmup = 300
      min_healthy_percentage = 30
    }
    triggers = ["launch_template", "tag"] 
  }
    tag {
      key  = "AMI-ID"
      value = data.aws_ami.pc_ami.image_id
      propagate_at_launch = false
    }

  #depends_on = [aws_launch_template.pet_ami_template]
}

/* ------------------------- auto scaling attachment ------------------------ */
resource "aws_autoscaling_attachment" "pet_autoscaling_attachment" {
  autoscaling_group_name = aws_autoscaling_group.pet_autoscaling_group.id # autoscaling_instance_group.id
  alb_target_group_arn = aws_lb_target_group.jass_lb_target_group.arn # Loadbalancing target group .arn
}

/* ------------------------------ load balancer ----------------------------- */
# Loadbalancer
resource "aws_lb" "jass_aws_lb" {
  name               = "jass-aws-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.jass_sg_lb
  subnets            = var.public_subnets_id

  enable_deletion_protection = false

  tags = {
    Environment = "production"
  }
  #   access_logs {
  #     bucket  = aws_s3_bucket.lb_logs.bucket
  #     prefix  = "test-lb"
  #     enabled = true
  #   }
}

/* ------------------------------ target group ------------------------------ */
# Target group
resource "aws_lb_target_group" "jass_lb_target_group" {
  name     = "jass-aws-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id #### aws_vpc id
}

/* ------------------------- load balancer listener ------------------------- */
resource "aws_lb_listener" "jass_front_end" {
  load_balancer_arn = aws_lb.jass_aws_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.jass_lb_target_group.arn
  }
}

/* -------------------------------- route 53 -------------------------------- */
resource "aws_route53_record" "petclinic_www" {
  zone_id = "Z03386713MCJ0LHJCA6DL" # AL academy 
  name    = "jass-pc"
  type    = "A"
  alias {
    name                   = aws_lb.jass_aws_lb.dns_name
    zone_id                = aws_lb.jass_aws_lb.zone_id
    evaluate_target_health = true
  }
}
# write to local file
resource "local_file" "route53_write" {
    content     = "${aws_route53_record.petclinic_www.name}.academy.labs.automationlogic.com"
    filename = "/home/ec2-user/route53dns"
}