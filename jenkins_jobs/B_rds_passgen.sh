#!/bin/bash
# Generate random password for RDS
python3 /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/python/passgen.py

# Posts new rds password timestamp to slack
echo "New RDS password generated. Timestamp: $(cat /home/ec2-user/TimeStamp_4_RDSpass)" > /home/ec2-user/passgen_log
python3 /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/python/slacker.py /home/ec2-user/passgen_log
