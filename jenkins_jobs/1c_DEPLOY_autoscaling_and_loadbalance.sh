#!/bin/bash

# -------- change directory to working directory of the master branch -------- #
cd /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/

# ----- executes tf plan for autoscaling and loadbalancer module. [as_lb] ---- #
# --------------------- Creates/Modifies as_lb resources --------------------- #
# additionally writes stdout to a file to be used for the slack notification.
#terraform plan -target=module.as_lb
terraform apply -target=module.as_lb -auto-approve 2>&1 | sudo tee /home/ec2-user/as_lb_output

# -- adds tf modifications to as_lb_log and posts contents of file to Slack -- #
touch /home/ec2-user/as_lb_log
:> /home/ec2-user/as_lb_log
# echo "[TERRAFORM] Create autoscaling group and loadbalancer, exit code: $?" >> /home/ec2-user/as_lb_log
echo -n $(echo "[TERRAFORM/AS_LB]: $(grep 'Apply complete' /home/ec2-user/as_lb_output)") >> /home/ec2-user/as_lb_log
python3 /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/python/slacker.py /home/ec2-user/as_lb_log

# ----------------------- slack message for petclic dns ---------------------- #
echo -n "[PET CLINIC] Application Ready:" > /home/ec2-user/route53dns_log
cat /home/ec2-user/route53dns >> /home/ec2-user/route53dns_log

# --------------- commit and push changes to terraform.tfstate --------------- #
git add .
git commit -m "tfstate change"
git push origin master

# --------------------- posts the pet clinic dns to slack -------------------- #
sleep 45s
python3 /home/ec2-user/workspace/0b_repo_a_manager/at_assessment3/python/slacker.py /home/ec2-user/route53dns_log