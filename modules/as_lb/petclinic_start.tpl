#!/bin/bash
# rm /home/ec2-user/petclinic/src/main/resources/application.properties

# Changing the application.properties file to include the correct database endpoint, username and password
sudo sed -i "s,spring.datasource.url=jdbc:mysql://localhost/petclinic, spring.datasource.url=jdbc:mysql://${dbhost}/petclinic," /home/ec2-user/petclinic/src/main/resources/new.properties
sudo sed -i "s,spring.datasource.username=petclinic, spring.datasource.username=${dbuser}," /home/ec2-user/petclinic/src/main/resources/new.properties
sudo sed -i "s,spring.datasource.password=petclinic, spring.datasource.password=${dbpass}," /home/ec2-user/petclinic/src/main/resources/new.properties
rm /home/ec2-user/petclinic/src/main/resources/application.properties
mv /home/ec2-user/petclinic/src/main/resources/new.properties /home/ec2-user/petclinic/src/main/resources/application.properties

# sending the data and scheme sql files to the rds database
mysql -h ${dbhost} -u ${dbuser} --password="${dbpass}" < /home/ec2-user/petclinic/src/main/resources/db/mysql/schema.sql
mysql -h ${dbhost} -u ${dbuser} --password="${dbpass}" < /home/ec2-user/petclinic/src/main/resources/db/mysql/data.sql
mysql -h ${dbhost} -u ${dbuser} --password="${dbpass}" < /home/ec2-user/create_user.sql

# starts the application using the init script
sudo chkconfig --add petclinic
sudo systemctl enable petclinic
sudo /etc/init.d/petclinic start
sudo systemctl start httpd