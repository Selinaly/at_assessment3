# PC webserver Security group

resource "aws_security_group" "jass_sg_webserver" {
  name = "jass_sg_webserver"
  description = "Allows 80 from LB and SSH from bastion"
  vpc_id = var.vpc_id
  ingress {
      from_port = var.http_port
      to_port = var.http_port
      protocol = "tcp"
      security_groups = [aws_security_group.jass_sg_loadbalancer.id]
      description = "Allows port 80 from loadbalancer"
    
    }

  ingress {
      from_port = var.ssh_port
      to_port = var.ssh_port
      protocol = "tcp"
      security_groups = [aws_security_group.jass_sg_bastion.id]
      description = "Allows port 22 from bastion"
  } 

  egress {
      from_port = var.outbound_port
      to_port = var.outbound_port
      protocol = "-1"
      cidr_blocks = var.open_internet
      ipv6_cidr_blocks = var.ipv6_cidr_block
    }
    tags = {
      "Name" = "JaSS-SG-PC-webserver"
    }
}      
# Loadbalancer SG

resource "aws_security_group" "jass_sg_loadbalancer" {
  name = "jass_sg_loadbalancer"
  description = "Allows port 80 all traffic and SSH from bastion"
  vpc_id = var.vpc_id
  ingress {
      from_port = var.http_port
      to_port = var.http_port
      protocol = "tcp"
      cidr_blocks = var.open_internet
      description = "Allows port 80"
    
    }

  ingress {
      from_port = var.ssh_port
      to_port = var.ssh_port
      protocol = "tcp"
      security_groups = [aws_security_group.jass_sg_bastion.id]
      description = "Allows port 22 from bastion"
  } 

  egress {
      from_port = var.outbound_port
      to_port = var.outbound_port
      protocol = "-1"
      cidr_blocks = var.open_internet
      ipv6_cidr_blocks = var.ipv6_cidr_block
    }
    tags = {
      "Name" = "JaSS-SG-Loadbalancer"
    }
} 
# RDS security group

resource "aws_security_group" "jass_db" {
  name = "jass_sg_db"
  description = "Allows port 3306"
  vpc_id = var.vpc_id
  ingress {
      from_port = var.mysql_port
      to_port = var.mysql_port
      protocol = "tcp"
      security_groups = [aws_security_group.jass_sg_webserver.id]
      description = "Allows port 3306"
    
  } 

  egress {
      from_port = var.outbound_port
      to_port = var.outbound_port
      protocol = "-1"
      cidr_blocks = var.open_internet
      ipv6_cidr_blocks = var.ipv6_cidr_block
    }
    tags = {
      "Name" = "JaSS-SG-Database"
    }
} 
# Jenkins Master SG

resource "aws_security_group" "jass_sg_jenkins_master" {
    name = "jass_sg_jenkins_master"
    description = "Allows SSH from JaSS IPs"
    tags = {
      "Name" = "JaSS_SG_Jenkins_Master"
    }
    vpc_id = var.vpc_id
    ingress {
      security_groups = [aws_security_group.jass_sg_home_ips.id]
      description = "Allows port 22 from JAMAINE"
      from_port = var.ssh_port
      protocol = "tcp"
      to_port = var.ssh_port
    }
    egress {
      cidr_blocks = var.open_internet
      from_port = var.outbound_port
      ipv6_cidr_blocks = var.ipv6_cidr_block
      protocol = "-1"
      to_port = var.outbound_port
    }
}
# Jenikins Worker SG

resource "aws_security_group" "jass_sg_bastion" {
    name = "jass_sg_bastion"
    description = "Allows SSH from Master SG"
    tags = {
      "Name" = "JaSS_SG_Bastion"
    }
    vpc_id = var.vpc_id
    ingress {
      security_groups = [aws_security_group.jass_sg_jenkins_master.id]
      description = "Allows port 22 from Jenkins Master SG"
      from_port = var.ssh_port
      protocol = "tcp"
      to_port = var.ssh_port
    }
    egress {
      cidr_blocks = var.open_internet
      from_port = var.outbound_port
      ipv6_cidr_blocks =var.ipv6_cidr_block
      protocol = "-1"
      to_port = var.outbound_port
    }
}

# All traffic from home IPs

resource "aws_security_group" "jass_sg_home_ips" {
    name = "jass_sg_home_ips"
    tags = {
      "Name" = "JaSS_SG_Home_IPs"
    }
    vpc_id = var.vpc_id

    ingress {
      cidr_blocks = [ "81.152.186.104/32", "81.152.188.68/32"]
      description = "Allows all traffic from JAMAINE"
      from_port = var.outbound_port
      protocol = "-1"
      to_port = var.outbound_port
    }
    ingress {
        cidr_blocks = ["95.147.37.162/32"]
        description = "Allows all traffic from SELINA"
        from_port = var.outbound_port
        protocol = "-1"
        to_port = var.outbound_port
    }
    ingress {
        cidr_blocks = ["152.37.90.101/32"]
        description = "Allows all traffic from STEPH"
        from_port = var.outbound_port
        protocol = "-1"
        to_port = var.outbound_port
    }
    ingress {
      cidr_blocks = [ "85.243.42.92/32", "94.63.101.60/32", "85.242.113.56/32", "95.136.65.200/32"]
      description = "Allows all traffic from FILIPE aka GEORGE"
      from_port = var.outbound_port
      protocol = "-1"
      to_port = var.outbound_port
    }
    egress {
      cidr_blocks = var.open_internet
      from_port = var.outbound_port
      ipv6_cidr_blocks = var.ipv6_cidr_block
      protocol = "-1"
      to_port = var.outbound_port
    }
}

resource "aws_security_group" "jass_bitbucket" {
      name = "jass_sg_bitbucket"
    tags = {
      "Name" = "JaSS_SG_Bitbucket"
    }
    vpc_id = var.vpc_id
    description = "Allows Bitbucket Webhook to run"
    ingress {
      cidr_blocks = ["13.52.5.96/28","13.236.8.224/28","18.136.214.96/28","18.184.99.224/28","18.234.32.224/28","18.246.31.224/28","52.215.192.224/28","104.192.137.240/28","104.192.138.240/28","104.192.140.240/28","104.192.142.240/28","104.192.143.240/28","185.166.143.240/28","185.166.142.240/28"]
      description = "Bitbucket IPs:80"
      from_port = 80
      protocol = "tcp"
      to_port = 80
    }

    ingress {
      cidr_blocks = ["13.52.5.96/28","13.236.8.224/28","18.136.214.96/28","18.184.99.224/28","18.234.32.224/28","18.246.31.224/28","52.215.192.224/28","104.192.137.240/28","104.192.138.240/28","104.192.140.240/28","104.192.142.240/28","104.192.143.240/28","185.166.143.240/28","185.166.142.240/28"]
      description = "Bitbucket IPs:8080"
      from_port = 8080
      protocol = "tcp"
      to_port = 8080
    }

    egress {
      cidr_blocks = var.open_internet
      from_port = var.outbound_port
      ipv6_cidr_blocks = var.ipv6_cidr_block
      protocol = "-1"
      to_port = var.outbound_port
    }
}