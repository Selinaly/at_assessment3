# Create aws_vpc

resource "aws_vpc" "jass_main_vpc" {
    cidr_block = var.vpc_cidr_block
    instance_tenancy = "default"
    enable_dns_hostnames = true

    tags = {
        Name = "JaSS VPC"
    }
}

# Create aws_public_subnet
resource "aws_subnet" "jass_public_subnet" {
  vpc_id     = aws_vpc.jass_main_vpc.id
  cidr_block = var.pub_block_1c
  map_public_ip_on_launch = true

  tags = {
    Name = "JaSS Public Subnet"
  }
}

resource "aws_subnet" "jass_public_subnet_1a" {
  vpc_id     = aws_vpc.jass_main_vpc.id
  cidr_block = var.pub_block_1a
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "JaSS Public Subnet 1a"
  }
}

resource "aws_subnet" "jass_public_subnet_1b" {
  vpc_id     = aws_vpc.jass_main_vpc.id
  cidr_block = var.pub_block_1b
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "JaSS Public Subnet 1b"
  }
}

# Create aws_private_subnet

resource "aws_subnet" "jass_private_subnet" {
  vpc_id     = aws_vpc.jass_main_vpc.id
  cidr_block = var.private_block_1c

  tags = {
    Name = "JaSS Private Subnet"
  }
}

resource "aws_subnet" "jass_private_subnet_1a" {
  vpc_id     = aws_vpc.jass_main_vpc.id
  cidr_block = var.private_block_1a
  availability_zone = "eu-west-1a"

  tags = {
    Name = "JaSS Private Subnet 1a"
  }
}

resource "aws_subnet" "jass_private_subnet_1b" {
  vpc_id     = aws_vpc.jass_main_vpc.id
  cidr_block = var.private_block_1b
  availability_zone = "eu-west-1b"

  tags = {
    Name = "JaSS Private Subnet 1b"
  }
}
# Create aws_internet_gateway

resource "aws_internet_gateway" "jass_igw" {
  vpc_id = aws_vpc.jass_main_vpc.id

  tags = {
    Name = "JaSS IGW"
  }
}

# Elastic IP for NAT Gateway
# resource "aws_eip" "jass_nat_eip" {
#   vpc = true
#   depends_on = [aws_internet_gateway.jass_igw]
  
#   tags = {
#       Name = "JaSS NAT Gateway EIP"
#   }
# }

# # Main NAT Gateway for VPC 
# resource "aws_nat_gateway" "jass_nat" {
#   allocation_id = aws_eip.jass_nat_eip.id
#   subnet_id = aws_subnet.jass_public_subnet.id

#   tags = {
#     Name = "JaSS NAT Gateway"
#   }
# }

# Route table for public subnet
resource "aws_route_table" "jass_public_rt" {
  vpc_id = aws_vpc.jass_main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.jass_igw.id
  }

  tags = {
    Name = "JaSS Public RT"
  }
}

# Association between public subnet and public route table
resource "aws_route_table_association" "jass_public" {
  subnet_id = aws_subnet.jass_public_subnet.id
  route_table_id = aws_route_table.jass_public_rt.id
}

resource "aws_route_table_association" "jass_public_1a" {
  subnet_id = aws_subnet.jass_public_subnet_1a.id
  route_table_id = aws_route_table.jass_public_rt.id
}

resource "aws_route_table_association" "jass_public_1b" {
  subnet_id = aws_subnet.jass_public_subnet_1b.id
  route_table_id = aws_route_table.jass_public_rt.id
}
# Route table for private subnet
resource "aws_route_table" "jass_private_rt" {
  vpc_id = aws_vpc.jass_main_vpc.id

  tags = {
    Name = "JaSS Private RT"
  }
}

# Association between private subnet and private route table
resource "aws_route_table_association" "jass_private" {
  subnet_id = aws_subnet.jass_private_subnet.id
  route_table_id = aws_route_table.jass_private_rt.id
}

resource "aws_route_table_association" "jass_private_1a" {
  subnet_id = aws_subnet.jass_private_subnet_1a.id
  route_table_id = aws_route_table.jass_private_rt.id
}

resource "aws_route_table_association" "jass_private_1b" {
  subnet_id = aws_subnet.jass_private_subnet_1b.id
  route_table_id = aws_route_table.jass_private_rt.id
}

# NACL for public
resource "aws_network_acl" "jass_public_nacl" {
  vpc_id = aws_vpc.jass_main_vpc.id
  subnet_ids = [ aws_subnet.jass_public_subnet.id, aws_subnet.jass_public_subnet_1a.id, aws_subnet.jass_public_subnet_1b.id]

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "JaSS Public NACL"
  }
}

# NACL for private subnet

resource "aws_network_acl" "jass_private_nacl" {
  vpc_id = aws_vpc.jass_main_vpc.id
  subnet_ids = [ aws_subnet.jass_private_subnet.id, aws_subnet.jass_private_subnet_1a.id, aws_subnet.jass_private_subnet_1b.id]

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.vpc_cidr_block
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "JaSS Private NACL"
  }
}